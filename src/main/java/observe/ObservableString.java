package observe;

public class ObservableString {
    private String value;
    private ObserverString observer;

    public void setValue(String s) {
        value = s;
        if (observer != null) {
            observer.onStringChanged(s);
        }
    }

    public void setStringObserver(ObserverString o) {
        observer = o;
    }

    public static void main(String[] args) {
        ObservableString observable = new ObservableString();

        ObserverString obs = s -> System.out.println("onStringChanged: " + s);
        observable.setStringObserver(obs);

        observable.setValue("Alice");
        observable.setValue("Munkaw");
        observable.setValue("Jessie");
    }
}