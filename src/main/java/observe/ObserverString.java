package observe;

public interface ObserverString {

    void onStringChanged(String s);
}
