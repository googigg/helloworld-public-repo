package lambda;


@FunctionalInterface
public interface MyInterface {

    int increment(int in);
    default int decrement(int in) {
      return in--;
    }
}
