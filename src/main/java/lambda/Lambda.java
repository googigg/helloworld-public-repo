package lambda;


public class Lambda {

    public static void main(String[] args) {
        /*
        MyInterface myInterface = new MyInterface() {
            @Override
            public int increment(int in) {
                return in++;
            }


            @Override
            public int decrement(int in) {
                return in--;
            }
        };
        */

        MyInterface result = in -> 2 + in;

        System.out.println(result.increment(4));
        System.out.println(result.decrement(5));
    }
}
