package thread;


import thread.tasks.OnFinishListener;
import thread.tasks.MyTask;

public class Main implements OnFinishListener {

    public static void main(String[] args) {
        MyTask myTask = new MyTask();
        myTask.setOnFinishListener(new Main());
        myTask.start();
    }

    @Override
    public void callbackMethod() {
        System.out.println("to do Something.");
    }
}
