package thread.tasks;


public class MyTask extends Thread implements Runnable {

    private OnFinishListener onFinishListener;

    public void setOnFinishListener(OnFinishListener listener) {
        this.onFinishListener = listener;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(onFinishListener != null)
            onFinishListener.callbackMethod();
    }
}
