package rx.cache;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;

public class Rx {

    static SingleObserver createSingleObserver() {
        return new SingleObserver<String>() {
            @Override
            public void onSubscribe(Disposable disposable) {

            }

            @Override
            public void onSuccess(String s) {
                System.out.println(s);
            }

            @Override
            public void onError(Throwable throwable) {

            }
        };
    }

    public static void main(String[] args) {
        Single<String> todosSingle = Single.create(emitter -> {
            Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(2000);

                    String responseFromWeb = "<todo></todo>";
                    System.out.println("Called to webservice once !!!\n");

                    emitter.onSuccess(responseFromWeb);
                } catch (Exception e) {
                    emitter.onError(e);
                }
            });
            thread.start();
        });

        // cache the result of the single, so that the web query is only done once
        Single<String> cachedSingle = todosSingle.cache();
        // Single<String> cachedSingle = todosSingle;

        cachedSingle.subscribeWith(createSingleObserver());
        cachedSingle.subscribeWith(createSingleObserver());
        cachedSingle.subscribeWith(createSingleObserver());
        cachedSingle.subscribeWith(createSingleObserver());
        cachedSingle.subscribeWith(createSingleObserver());
    }
}
