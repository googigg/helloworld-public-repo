package rx.filter;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class Rx {

    private static ExecutorService forObserverThread = Executors.newSingleThreadExecutor();
    private static List<String> list = new ArrayList<>();

    static {
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("six");
        list.add("seven");
        list.add("eight");
        list.add("nine");
        list.add("ten");
    }

    static void doSomething(String in) {
        String threadName = Thread.currentThread().getName();
        System.out.println("thread: " + threadName + " " + in);
    }

    static Observer createObserver() {
        return new Observer<String>() {
            @Override
            public void onSubscribe(Disposable disposable) {

            }

            @Override
            public void onNext(String o) {
                doSomething(o);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onComplete() {
                doSomething("all be emitted.");
            }
        };
    }


    public static void main(String[] args) throws Exception {

        /*
        List<String> newList = list.stream().filter(s -> s.length() == 3).limit(2).map(s -> s + ".suffix").collect(Collectors.toList());
        newList.forEach(item -> doSomething(item));
        System.out.println("\n\n");
        */

        // Rx is single-threaded by default
        Observable observable = Observable.just(list)
                .flatMapIterable(s -> s)
                .filter(s -> s.length() == 3)
                .take(2)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.from(forObserverThread))
                .map(s -> s + ".suffix");

        observable.subscribeWith(createObserver());
        // System.out.println(observable.blockingLast());

        System.in.read();
    }
}
