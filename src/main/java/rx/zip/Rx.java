package rx.zip;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;

public class Rx {

    private static List<String> list1 = new ArrayList<>();

    static {
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("4");
        list1.add("5");
    }

    private static List<String> list2 = new ArrayList<>();

    static {
        list2.add("A");
        list2.add("B");
        list2.add("C");
        list2.add("D");
    }

    static void doSomething(String in) {
        System.out.println(in);
    }


    static Observer createObserver() {
        return new Observer<String>() {
            @Override
            public void onSubscribe(Disposable disposable) {

            }

            @Override
            public void onNext(String o) {
                doSomething(o);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {
                doSomething("All was emitted.");
            }
        };
    }

    public static void main(String[] args) {

        Observable observable1 = Observable.just(list1).flatMapIterable(s -> s);
        Observable observable2 = Observable.just(list2).flatMapIterable(s -> s);

        Observable<String> zipped = Observable.zip(observable1, observable2, (x, y) -> x + "" + y);
        zipped.subscribeWith(createObserver());

        // http://rxmarbles.com/#zip
    }
}
