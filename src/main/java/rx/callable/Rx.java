package rx.callable;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

import java.io.IOException;
import java.util.ArrayList;

public class Rx {

    static Pet getPetWithDelayInMilliSec(int sleep, String name) {
        System.out.println("hello, " + name + " by thread: " + Thread.currentThread().getName());
        try {
            // simulate long task, with sleep
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new Pet(name);
    }

    public static void main(String[] args) {
        // Rx is single-threaded by default
        Observable<Pet> teddyObservable = Observable.fromCallable(() -> getPetWithDelayInMilliSec(3000, "teddy"))
                                                    .subscribeOn(Schedulers.io());
        Observable<Pet> kittyObservable = Observable.fromCallable(() -> getPetWithDelayInMilliSec(4000, "kitty"))
                                                    .subscribeOn(Schedulers.io());

        Observable<Farm> zipped = Observable.zip(teddyObservable, kittyObservable, (teddy, kitty) -> {
            Farm farm = new Farm();
            farm.add(kitty);
            farm.add(teddy);

            return farm;
        });

        zipped.subscribe(result -> result.displayAllPet());

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Getter
    static class Pet {

        private String name;

        public Pet(String name) {
            this.name = name;
        }
    }

    static class Farm extends ArrayList {

        public void displayAllPet() {
            System.out.print("\n All pet, we have");
            this.forEach(item -> System.out.print(" " + ((Pet)item).getName()));
        }
    }
}
